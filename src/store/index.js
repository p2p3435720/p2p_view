import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate";

export default createStore({
  plugins: [createPersistedState()],

  state: {
    MOBILE_OTP: '',
    EMAIL_OTP: '',
    R_KEY: '',
    VM:{},
    USER_EMAIL:'',
    USER_MOBILE:'',
    LOGIN_TYPE:''
  },

  mutations: {
    SET_MOBILE_OTP(state, payload) {
      state.MOBILE_OTP = payload
    },
    SET_EMAIL_OTP(state, payload) {
      state.EMAIL_OTP = payload
    },
    
    SET_R_KEY(state,payload){
      state.R_KEY = payload
    },
    SET_VM(state,payload){
      state.VM = payload
    },
    SET_USER_EMAIL(state,payload){
      state.USER_EMAIL = payload
    },
    SET_USER_MOBILE(state,payload){
      state.USER_MOBILE = payload
    },
    SET_LOGIN_TYPE(state,payload){
      state.LOGIN_TYPE = payload
    },
  },
  getters: {
    getMobileOtp: (state) => {
      return state.MOBILE_OTP
    },
    getEmailOtp: (state) => {
      return state.EMAIL_OTP
    },
    getR_key: (state) => {
      return state.R_KEY
    },
    getVM: (state) => {
      return state.VM
    },
    getUserEmail: (state) => {
      return state.USER_EMAIL
    },
    getUserMobile: (state) => {
      return state.USER_MOBILE
    },
    getLoginType: (state) => {
      return state.LOGIN_TYPE
    },
    
   
  },

})
