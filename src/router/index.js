import { createRouter, createWebHistory } from "vue-router";
import LoginView from "../views/auth/LoginView.vue";
import { p2p_routes } from '../P2P/router'

const routes = [ 

    {
        path: "/about",
        name: "about",
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/AboutView.vue"),
    },

    {
        path: "/",
        name: "LoginView",
        component: () =>
            import ( /* webpackChunkName: "LoginView" */ "../views/auth/LoginView.vue"),
            meta: {authOnly: false}
    },

    {
        path: "/create",
        name: "CreatePage",
        component: () =>
            import ( /* webpackChunkName: "CreatePage" */ "../views/auth/CreatePage.vue"),
    },
    {
        path: "/resetpassword",
        name: "ResetPassword",
        component: () =>
            import ( /* webpackChunkName: "CreatePage" */ "../views/auth/ResetPassword.vue"),
    },
    {
        path: "/passwordsecurityverify",
        name: "PasswordSecurityVerify.vue",
        component: () =>
            import ( /* webpackChunkName: "CreatePage" */ "../views/auth/PasswordSecurityVerify.vue"),
    },
    {
        path: "/changepassword",
        name: "ChangePassword.vue",
        component: () =>
            import ( /* webpackChunkName: "CreatePage" */ "../views/auth/ChangePassword.vue"),
    },
    {
        path: "/securityverify",
        name: "SecurityVerify",
        component: () =>
            import ( /* webpackChunkName: "CreatePage" */ "../views/auth/SecurityVerify.vue"),
    },
    {
        path: "/verification",
        name: "VerificationPage",
        component: () =>
            import ( /* webpackChunkName: "CreatePage" */ "../views/auth/VerificationPage.vue"),
    },

];

let All_routes = [...routes, ...p2p_routes];

const router = createRouter({
    history: createWebHistory("/"),
    routes: All_routes,
});

router.beforeEach((to, from, next) => {
    const loggedIn = localStorage.getItem("token");
    const isAuth = to.matched.some((record) => record.meta.authOnly);
    if (isAuth && !loggedIn) {
      return next({ path: "/" });
    } else if (!isAuth && loggedIn) {
      return next({ path: "/p2p" });
    } else if (!isAuth && !loggedIn) {
      return next();
    }
    next();
  });


export default router;