export const p2p_routes = [
  {
    path: "/p2p",
    name: "p2p",
    component: () =>
      import(/* webpackChunkName: "p2p" */ "./views/P2PView.vue"),
      meta: {authOnly: true}
  },
  
 
    {
    path: "/orders",
    name: "orders",
    component: () =>
      import(/* webpackChunkName: "about" */ "../P2P/views/OrderView.vue"),
  },
  {
    path: "/advertiser-details",
    name: "advertiser-details",
    component: () =>
      import(/* webpackChunkName: "about" */ "../P2P/views/AdvertiserDetailsView.vue"),
  },
  {
    path: "/postnormal",
    name: "Post Normal",
    component: () =>
      import(/* webpackChunkName: "Post Normal" */ "./views/PostNormal.vue"),
  },
  {
    path: "/myads",
    name: "My Ads",
    component: () =>
      import(/* webpackChunkName: "My Ads" */ "./views/MyAds.vue"),
  },
  {
    path: "/payment",
    name: "payment",
    component: () =>
      import(/* webpackChunkName: "payment" */ "./views/PaymentView.vue"),
  },
  {
    path: "/Add",
    name: "PAYTM",
    component: () =>
      import(/* webpackChunkName: "PAYTM" */ "./views/AddPayment.vue"),
  },
  // {
  //   path: "/orderdetail",
  //   name: "orderdetail",
  //   component: () =>
  //     import(/* webpackChunkName: "orderdetail" */ "./views/OrderDetailView.vue"),
  // },
  {
    path: "/buy_detail",
    name: "Buy Detail",
    component: () =>
      import(/* webpackChunkName: "orderdetail" */ "./views/BuyDetailView.vue"),
  },
  {
    path: "/sell_detail",
    name: "Sell Detail",
    component: () =>
      import(/* webpackChunkName: "Sell Detail" */ "./views/SellDetailView.vue"),
  },
  {
    path: "/orderstatus",
    name: "orderstatus",
    component: () =>
      import(/* webpackChunkName: "orderstatus" */ "./views/OrderStatusView.vue"),
  },
];
